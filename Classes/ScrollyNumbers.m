//
//  ScrollyNumbers.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "ScrollyNumbers.h"


@implementation ScrollyNumbers

- (void) Initialise
{
	_whitemode = false;
	_lastcolvisible = true;
	[self InitialiseFrames:false];
	[self InitialiseFrames:true];
	
}

- (void) setLocation:(CGPoint)location
{
	_location = location;
	
	_xscreenoffset = _location.x;
	_yscreenoffset = _location.y;
}

- (void) setLastcolumnvisible:(bool)bvisible
{
	_lastcolvisible = bvisible;
}

- (void) InitialiseFrames:(bool)whiteFrames
{
	_location = CGPointMake(190.0f, 600.0f); // default location
	
	UIImage *scrollynumbers;
	
	if (whiteFrames == true) {
		scrollynumbers = [UIImage imageNamed:@"ScrollyNumberWhite.png"];
	} else {
		scrollynumbers = [UIImage imageNamed:@"ScrollyNumberPlain.png"];
	}
	
	
	CGRect imageRect;
	_endframe = 1;
	
	_xscreenoffset = _location.x;
	_yscreenoffset = _location.y;
	
	_entireRect = CGRectMake(_xscreenoffset, _yscreenoffset, 624, 141);
	
	// Build the array of frames for the animation
	if (whiteFrames == true) {
		_whiteFrames = [NSMutableArray new];
	} else {
		_blueFrames = [NSMutableArray new];
		_frameLocations = [NSMutableArray new];
	}
	
	
	
	UIImage *frame1;
	
	CGFloat startx = 1.0f;
	CGFloat starty = 5.0f;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	CGPoint currentFramelocation;
	FrameLocation *curLocation;
	
	
	int i = 0;
	int h = 0;
	
	for (h = 0;h<7;++h) {
		
		for (i = 0;i<10;++i) {
			
			curLocation = [FrameLocation new];
			currentFramelocation = CGPointMake(startx + xoffset, starty + yoffset);
			[curLocation setLocation:currentFramelocation];
			
			imageRect.origin = currentFramelocation;
			imageRect.size = CGSizeMake(79.0f, 14.0f);
			frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([scrollynumbers CGImage], imageRect)];
			
			if (whiteFrames == true) {
				[_whiteFrames addObject:frame1];
				[curLocation release];
			} else {
				[_blueFrames addObject:frame1];	
				[_frameLocations addObject:curLocation];
			}
			
			
			
			
			yoffset+=13.0f;
		}
		
		xoffset += 78.0f;
		yoffset = 0.0f;
	}
}


- (CGRect) getEntirerect
{
	return _entireRect;
}

- (bool) isFramelastcolumn:(int)framenumber
{
	int lastframe = [_frameLocations count];
	int beginningframelastcol = lastframe - 20;
	
	if (framenumber >= beginningframelastcol && framenumber <= lastframe)
	{
		return true;
	} else {
		return false;
	}
}

- (CGPoint) getActualframelocation:(int)FrameNumber
{	
	CGPoint curPoint;
	CGPoint resPoint;
	
	resPoint = CGPointMake(0.0f, 0.0f);
	
	if (FrameNumber < [_frameLocations count])
	{
		
		curframe = [_frameLocations objectAtIndex:FrameNumber];
		
		curPoint = [curframe getLocation];
		
		resPoint = CGPointMake(_xscreenoffset + curPoint.x, _yscreenoffset + curPoint.y);
		
	}
	
	return resPoint;
}

- (void) Update
{
	
	if (_whitemode == false) {
		_endframe++;
		if (_endframe > ([_blueFrames count] -1)) {
			_endframe = 1;
		
			if (_whitemode == false) {
				_whitemode = true;
				_whiteframetarget = arc4random() % [_whiteFrames count];
				if (_whiteframetarget > ([_whiteFrames count] - 1)) {
					_whiteframetarget = [_whiteFrames count] -1;
				}
			}
		}
	} else {
		if (_endframe > _whiteframetarget) {
			//_endframe = 1;
			
			
			//_whitemode = false;
			_flashmode = true;
			
		} else {
			_endframe++;
		}
	}
	
	if (_flashmode == true) {
		_flashcount++;
		
		if (_flashcount > 14) {
			_endframe = 1;
			_flashcount = 0;
			_flashon = false;
			_flashmode = false;
			_whitemode = false;
		}
		
		
		if (_flashon == false) {
			_flashon = true;
		} else {
			_flashon = false;
		}
	}
}

- (void) Paint
{
	
	bool showflash = true;
	int f = 0;
	
	if (_whitemode == false) {
		
		if (_endframe < [_blueFrames count]) {
	
			for (f=0;f<_endframe;++f) {
	
				if (_lastcolvisible == false) {
					
					if ([self isFramelastcolumn:f] == false) {
						frame = [_blueFrames objectAtIndex:f];
						
						if (frame != nil) {
							[frame drawAtPoint:[self getActualframelocation:f]];
						}
					}
				} else {
					frame = [_blueFrames objectAtIndex:f];
					
					if (frame != nil) {
						[frame drawAtPoint:[self getActualframelocation:f]];
					}
				}
				
	
			}
		}
	
	} else {
		
		for (f=0;f<[_blueFrames count];++f) {
			
			if (_lastcolvisible == false) {
				if ([self isFramelastcolumn:f] == false) {
					frame = [_blueFrames objectAtIndex:f];
					if (frame != nil) {
						[frame drawAtPoint:[self getActualframelocation:f]];
					}
				}
			} else {
				frame = [_blueFrames objectAtIndex:f];
				if (frame != nil) {
					[frame drawAtPoint:[self getActualframelocation:f]];
				}
			}
			
			
		}
		
		
		if (_lastcolvisible == false) {
			if ([self isFramelastcolumn:_endframe] == true) {
				showflash = false;
			}
		}
		
		
		if (showflash == true) {
			if (_flashmode == true) {
		
				if (_flashon == true) {
					if (_endframe < [_whiteFrames count]) {
						frame = [_whiteFrames objectAtIndex:_endframe];
						if (frame != nil) {
							[frame drawAtPoint:[self getActualframelocation:_endframe]];
						}
					}
				} else {
					if (_endframe < [_blueFrames count]) {
						frame = [_blueFrames objectAtIndex:_endframe];
						
						if (frame != nil) {
							[frame drawAtPoint:[self getActualframelocation:_endframe]];
						}
					}
				}
				
			} else {
				if (_endframe < [_whiteFrames count]) {
					frame = [_whiteFrames objectAtIndex:_endframe];
					
					if (frame != nil) {
						[frame drawAtPoint:[self getActualframelocation:_endframe]];
					}
				}
			}
		}
		
		
		
	}
	//frame = [_blueFrames objectAtIndex:1];
	//[frame drawAtPoint:[self getActualframelocation:1]];
}

@end
