//
//  LCARS_ClockViewController.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ClockView.h"

@interface LCARS_ClockViewController : UIViewController {

	IBOutlet ClockView *clockview;
}

@property (retain, nonatomic) ClockView *clockview;

- (void) saveSettings;

@end

