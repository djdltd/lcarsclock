//
//  FrameLocation.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FrameLocation : NSObject {
	CGPoint location;
}

- (CGPoint) getLocation;
- (void) setLocation:(CGPoint)loc;

@end
