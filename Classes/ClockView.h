//
//  ClockView.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollyNumbers.h"
#import "ClockNumbers.h"
#import "SecondNumbers.h"
#import "AlarmNumbers.h"
#import "SmallNumbers.h"
#import "DaysTemplate.h"
#import "MonthsTemplate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>

typedef enum {
	HourUp,
	HourDown,
	MinuteUp,
	MinuteDown,
	AlarmSound1,
	AlarmSound2,
	AltAlarmSound,
	AlarmOnOff,
	AltAlarmOnOff,
	HourMode,
	AlertSnooze,
	AlertDismiss,
	None
} ButtonIdentifier;

typedef enum {
	AdjHourUp,
	AdjHourDown,
	AdjMinuteUp,
	AdjMinuteDown,
	AdjSecondUp,
	AdjSecondDown,
	AdjNone
} AlarmAdjustMode;

typedef enum {
	SndClockButton,
	SndClockButtonHigh,
	SndAlarmSound1,
	SndAlarmSound2,
	SndAlarmSound3,
	SndAlarmSound4,
	SndAlarmSound5,
	SndAlarmSound6,
	SndAlarmSound7,
	SndAlarmSound8,
	SndAlarmSound9,
	SndAlarmSound10
} ClockSound;

@interface ClockView : UIView<AVAudioPlayerDelegate> {
	
	IBOutlet UIButton *btn_hourup;
	IBOutlet UIButton *btn_hourdown;
	IBOutlet UIButton *btn_minuteup;
	IBOutlet UIButton *btn_minutedown;
	IBOutlet UIButton *btn_secondup;
	IBOutlet UIButton *btn_seconddown;
	IBOutlet UIButton *btn_altsound;
	IBOutlet UIButton *btn_alarm;
	IBOutlet UIButton *btn_alarmtime;
	IBOutlet UIButton *btn_starttimer;
	IBOutlet UIButton *btn_stoptimer;
	IBOutlet UIButton *btn_alarmmode;
	IBOutlet UIButton *btn_timermode;
	IBOutlet UIButton *btn_timerduration;
	
	IBOutlet UIImageView *imgview_alarmactive;
	IBOutlet UIImageView *imgview_ampmindicator;
	IBOutlet UIImageView *imgview_topbar;
	IBOutlet UIImageView *imgview_lowerleftbar;
	
	//UIImage *alarmsound1label;
	//UIImage *alarmsound2label;
	
	UIImage *alarmsoundlabel;
	
	//UIImage *alarmactivelabel;
	//UIImage *alarmofflabel;
	
	UIImage *alert;
	UIImage *alertsnoozehigh;
	UIImage *alertsnoozenorm;
	UIImage *alertdismisshigh;
	UIImage *alertdismissnorm;
	
	//UIImage *img_am;
	//UIImage *img_pm;
	
	CGPoint _background_location;
	//CGPoint _alarmtimebutton_location;
	//CGPoint _alarmactivelabel_location;
	CGPoint _alarmsoundlabel_location;
	//CGPoint _ampmlocation;
	CGPoint _alert_location;
	

	
	CGRect _alertsnoozebutton_rect;
	CGRect _alertdismissbutton_rect;
	

	
	
	ScrollyNumbers *_scrollynumbers;
	ClockNumbers *_clocknumbers;
	SecondNumbers *_secondnumbers;
	AlarmNumbers *_alarmnumbers;
	
	SmallNumbers *_daynumbers;
	DaysTemplate *_daystemplate;
	SmallNumbers *_yearnumbers;
	MonthsTemplate *_monthstemplate;
	SmallNumbers *_stardatenumbers;
	
	NSDateFormatter* _formatter;
	
	//bool _alarmsound1;
	bool _alarmactive;
	

	
	int _currentalarmminute;
	int _currentalarmhour;
	
	int _currenttimerhour;
	int _currenttimerminute;
	int _currenttimersecond;
	
	int _backuptimerhour;
	int _backuptimerminute;
	int _backuptimersecond;
	
	int _currentminute;
	int _currenthour;
	bool _12hourmode;
	
	int _alarmbuttonrepeat;
	int _repeatspeed;
	bool _firsttap;
	bool _alertactive;
	bool _alerttextvisible;
	int _alertrepeatcount;
	bool _snoozemode;
	bool _backgrounddrawn;
	bool _audioplaying;
	
	bool _dismissbuttondown;
	bool _snoozebuttondown;
	bool _userdismissedalert;
	bool _ispm;
	
	// These variables are so that when you drag away from a button the toggle does not constantly go back and fourth
	bool _alarmtoggleset;
	bool _hourmodetoggleset;
	bool _soundtoggleset;
	
	NSLocale *_uslocale;
	NSCalendar *_gregorian;
	NSInteger _numericmonth;
	NSInteger _numericweekday;
	int _currentbrightness;
	
	NSMutableString *_strtime;
	
	//SystemSoundID _ssbutton;
	//SystemSoundID _ssbuttonhigh;
	//SystemSoundID _ssalarm1;
	//SystemSoundID _ssalarm2;
	
	SystemSoundID _timeannounce;
	
	ButtonIdentifier _lastbuttontouched;
	int _ytest;
	bool _landscapemode;
	bool _appinitialised;
	bool _timeannouncementplaying;
	int _iannouncenumber;
	bool _alarmadjustedbytimer;
	bool _timermode;
	bool _timeractive;
	
	//NSTimer *clockTimer;
	NSTimer *animationTimer;
	NSTimer *announceTimer;
	NSTimer *snoozeTimer;
	NSTimer *_alarmadjusttimer;
	NSTimer *_timertimer;
	
	AVAudioPlayer* _myaudioplayer;
	
	AlarmAdjustMode _alarmadjustmode;
	int _currentalarmsound;
}

@property (retain, nonatomic) UIButton *btn_hourup;
@property (retain, nonatomic) UIButton *btn_hourdown;
@property (retain, nonatomic) UIButton *btn_minuteup;
@property (retain, nonatomic) UIButton *btn_minutedown;
@property (retain, nonatomic) UIButton *btn_altsound;
@property (retain, nonatomic) UIButton *btn_alarm;
@property (retain, nonatomic) UIButton *btn_alarmtime;
@property (retain, nonatomic) UIButton *btn_secondup;
@property (retain, nonatomic) UIButton *btn_seconddown;
@property (retain, nonatomic) UIButton *btn_starttimer;
@property (retain, nonatomic) UIButton *btn_stoptimer;
@property (retain, nonatomic) UIButton *btn_timermode;
@property (retain, nonatomic) UIButton *btn_alarmmode;
@property (retain, nonatomic) UIButton *btn_timerduration;
@property (retain, nonatomic) UIImageView *imgview_alarmactive;
@property (retain, nonatomic) UIImageView *imgview_ampmindicator;
@property (retain, nonatomic) UIImageView *imgview_topbar;
@property (retain, nonatomic) UIImageView *imgview_lowerleftbar;





- (void)play12Hournumeric:(int)numeric;
- (void)RefreshTimertime;
- (void)loadAlarmsoundlabel:(int)soundnumber;
- (IBAction)alarmAdjustcancel:(id)sender;
- (void)alarmAdjust:(AlarmAdjustMode)adjustmode;
- (void) alarmAdjusttick:(id)sender;
- (void)startAlarmadjust;
- (void)snoozeTick:(id)sender;
- (void)startSnooze;
- (bool)isPointWithinRectEx:(CGFloat)x:(CGFloat)y:(CGRect)rect;
- (void)checkButtontouched:(CGFloat)x:(CGFloat)y:(bool)checkreleased;
- (void)checkButtonmoved:(CGFloat)x:(CGFloat)y:(bool)checkreleased;
- (void)drawView:(id)sender;
- (void)triggerAnnounce:(id)sender;
- (void)clockTick:(id)sender;
- (void)startAnimation;
- (void)refreshClockTick;
- (void)GetNumericTime;
- (void)Allbuttonsreleased;
- (void)SecondAdjust:(bool)down;
- (void)MinuteAdjust:(bool)down;
- (void)HourAdjust:(bool)down;
- (void)RefreshAlarmtime;
- (void)setLandscapemode:(bool)landscape;
- (void)setupImagelocations:(bool)landscape;
- (void)saveSettings;
- (void)loadSettings;
- (void)playSinglesound:(NSString *)resourcename;
- (void)playCurrentalarmsound;
- (void)playSound:(ClockSound)soundtoplay;
- (void)timerTick:(id)sender;
- (void)startTimer;
- (void)stopTimer;
- (void) configureAudioServices;
- (void)playSinglesoundEx:(NSString *)resourcename;


- (void)refreshAlarmactive_imageview;
- (void)refreshAmpm_imageview;

- (IBAction)btnAdjustbrightness:(id)sender;
- (IBAction)timerModebuttontriggered:(id)sender;
- (IBAction)alarmModebuttontriggered:(id)sender;
- (IBAction)starttimerbuttontriggered:(id)sender;
- (IBAction)starttimerbuttonreleased:(id)sender;
- (IBAction)stoptimerbuttontriggered:(id)sender;
- (IBAction)stoptimerbuttonreleased:(id)sender;

- (IBAction)alarmSoundBackbuttontriggered:(id)sender;
- (IBAction)alarmSoundNextbuttontriggered:(id)sender;
- (IBAction)altAlarmsoundbuttontriggered:(id)sender;
- (IBAction)altAlarmsoundbuttonreleased:(id)sender;

- (IBAction)hourUpbuttontriggered:(id)sender;
- (IBAction)hourDownbuttontriggered:(id)sender;
- (IBAction)minuteUpbuttontriggered:(id)sender;
- (IBAction)minuteDownbuttontriggered:(id)sender;

- (IBAction)hourUpbuttonreleased:(id)sender;
- (IBAction)hourDownbuttonreleased:(id)sender;
- (IBAction)minuteUpbuttonreleased:(id)sender;
- (IBAction)minuteDownbuttonreleased:(id)sender;

- (IBAction)secondUpbuttonreleased:(id)sender;
- (IBAction)secondDownbuttonreleased:(id)sender;
- (IBAction)secondUpbuttontriggered:(id)sender;
- (IBAction)secondDownbuttontriggered:(id)sender;

- (IBAction)alarmOnOffbuttontriggered:(id)sender;
- (IBAction)alarmOnOffbuttonreleased:(id)sender;
- (IBAction)hourModebuttontriggered:(id)sender;
- (IBAction)hourModebuttonreleased:(id)sender;
- (IBAction)alarmtimermodebuttonreleased:(id)sender;

- (IBAction)announcebuttonPressed:(id)sender;
- (void)startAnnouncementTimer;
- (void)playNumericsound:(int)numeric;
- (void)playSystemsound:(NSString *)resourcename;

// Local Notification Stuff
- (void)scheduleGenericalarm;
- (void)clearAllLocalnotifications;
- (void)scheduleAlarmForDate:(NSDate*)theDate;


@end
