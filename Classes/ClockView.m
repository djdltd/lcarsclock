//
//  ClockView.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

// Version 1.1
// version 1.1 Fixes:

/*
Bug fix for date not displaying on international configurations
Bug fix whereby buttons would get stuck in the pressed state

Added an AM/PM indicator
Changed the Title to LCARS CLOCK

Portrait mode is now supported in addition to landscape mode
Settings are now retained when you exit the app
Setting the alarm now produces a single beep per button.
The Alarm and Sound buttons on the right hand side are now functional.

New Feature: Added a voice announcement feature that speaks the time when the announce time button is pressed.

// Version 1.2
// Version 1.2 Fixes:
 
Added StarDate to the top alongside the date
 
Fixed a bug whereby the Voice Announcement would announce the time in 24 hour mode if the clock was set to 12 hour mode.
 
Added 8 more sounds. The LCARS Clock now has 10 alarm sounds. The alarm sound buttons on the left have been changed
to Next and Back buttons allowing easy selection of the various sounds.
 
A Timer Feature has been added. It is now possible to switch between alarm mode and timer mode. You can set the timer duration,
start and stop the timer. When the Timer Duration expires the alarm will be triggered using the currently selected alarm sound.
 
Fixed a stability issue that would manifest if the iPad was low on memory.

// Version 1.2.1
// Version 1.2.1 Fixes:
 
Fixed a bug whereby the AM/PM status indicator was not being updated correctly.

// Version 1.2.2
// Version 1.2.2 Fixes:
 
Fixed an additional bug with the AM/PM status whereby the AM/PM status was not being correctly recognised
if the 24 hour mode in the iPad Date/Time settings were switched off
 
(All that was needed here was to explicitly set the locale of the date formatter to en_US - aarrgh!!
 
// Version 1.2.3
// Version 1.2.3 Fixes:
 
The sound playing is now done using an Audio Session. This is better then relying on the system sound player
and also fixes a problem whereby the alarm would not sound if the iPad was locked.
 
Added a Brightness Adjustment feature. Tap the Brightness button to toggle between 5 brightness levels.

The iPad status bar is now hidden whilst the app is running.
 
// Version 1.2.4
// Version 1.2.4 Fixes:
 
Fixed a bug whereby the alarm and clock settings were not being saved.
 
Fixed a bug whereby the clock would display the incorrect year on 1st Jan.

 
// Version 1.2.5
// Version 1.2.5 Fixes:
 
The alarm will now work when the LCARS Clock is running in the background.
 
*/


#import "ClockView.h"


@implementation ClockView

@synthesize btn_hourup; 
@synthesize btn_hourdown; 
@synthesize btn_minuteup;
@synthesize btn_minutedown;
@synthesize btn_altsound;
@synthesize btn_alarm;
@synthesize btn_alarmtime;
@synthesize btn_secondup;
@synthesize btn_seconddown;
@synthesize btn_starttimer;
@synthesize btn_stoptimer;
@synthesize btn_alarmmode;
@synthesize btn_timermode;
@synthesize btn_timerduration;
@synthesize imgview_alarmactive;
@synthesize imgview_ampmindicator;
@synthesize imgview_topbar;
@synthesize imgview_lowerleftbar;

- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		
		_landscapemode = true;
		_appinitialised = false;
		
		
		/*
		if (alarmsound1label == nil)
		{
			alarmsound1label = [UIImage imageNamed:@"Snd1.png"];
		}	
		
		if (alarmsound2label == nil)
		{
			alarmsound2label = [UIImage imageNamed:@"Snd2.png"];
		}
		*/
		
		/*
		if (alarmactivelabel == nil)
		{
			alarmactivelabel = [UIImage imageNamed:@"Active.png"];
		}	
		
		if (alarmofflabel == nil)
		{
			alarmofflabel = [UIImage imageNamed:@"Off.png"];
		}
		 */
		
		/*
		if (img_am == nil)
		{
			img_am = [UIImage imageNamed:@"AM.png"];
		}
		
		if (img_pm == nil)
		{
			img_pm = [UIImage imageNamed:@"PM.png"];
		}
		*/
		
		if (alert == nil)
		{
			[alert = [UIImage imageNamed:@"AlarmAlert.png"] retain];
		}
		
		if (alertsnoozehigh == nil)
		{
			[alertsnoozehigh = [UIImage imageNamed:@"SnoozeHigh.png"] retain];
		}
		
		if (alertsnoozenorm == nil)
		{
			[alertsnoozenorm = [UIImage imageNamed:@"SnoozeNorm.png"] retain];
		}
		
		if (alertdismisshigh == nil)
		{
			[alertdismisshigh = [UIImage imageNamed:@"DismissHigh.png"] retain];
		}
		
		if (alertdismissnorm == nil)
		{
			[alertdismissnorm = [UIImage imageNamed:@"DismissNorm.png"] retain];
		}
		
		[_gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] retain];
		
		
		
		
		[_strtime = [NSMutableString new] retain];
		_alarmactive = false;
		//_alarmsound1 = true;
		_alertactive = false;
		_backgrounddrawn = false;
		_12hourmode = false;
		_timermode = false;
		_timeractive = false;
		
		
		_userdismissedalert = false;
		 
		_dismissbuttondown = false;
		_snoozebuttondown = false;
		
		[_scrollynumbers = [ScrollyNumbers new] retain];
		[_scrollynumbers Initialise];
		
		
		[_clocknumbers = [ClockNumbers new] retain];
		[_clocknumbers Initialise];
				
		[_alarmnumbers = [AlarmNumbers new] retain];
		[_alarmnumbers Initialise];
		
		[_daynumbers = [SmallNumbers new] retain];
		[_daynumbers Initialise];
		[_daynumbers setLocation:200.0f :190.0f];
		
		[_yearnumbers = [SmallNumbers new] retain];
		[_yearnumbers Initialise];
		
		[_stardatenumbers = [SmallNumbers new] retain];
		[_stardatenumbers Initialise];
		[_stardatenumbers setLocation:525.0f :100.0f];
		
		[_daystemplate = [DaysTemplate new] retain];
		[_daystemplate Initialise];
		[_daystemplate setLocation:200.0f :100.0f];
		
		[_monthstemplate = [MonthsTemplate new] retain];
		[_monthstemplate Initialise];
		[_monthstemplate setLocation:260.0f :190.0f];
		
		_currenttimerhour = 0;
		_currenttimerminute = 5;
		_currenttimersecond = 0;
		
		_currentalarmhour = 7;
		_currentalarmminute = 30;
		_alarmbuttonrepeat = 0;
		_currentalarmsound = 1;
		_repeatspeed = 5;
		_firsttap = false;
		_snoozemode = false;
		_currentbrightness = 5;
		
		_alerttextvisible = false;
		_alertrepeatcount = 0;
		
		[self loadSettings];
		
		[self loadAlarmsoundlabel:_currentalarmsound];
		
		[self RefreshAlarmtime];
		
		[_secondnumbers = [SecondNumbers new] retain];
		[_secondnumbers Initialise];
		
		[_formatter = [NSDateFormatter new] retain];
		
		[_uslocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] retain];
		[_formatter setLocale:_uslocale];
		
		// Set up the initial image locations for landscape, but hopefully the device
		// orientation method will trigger this method again setting the correct
		// orientation
		[self setupImagelocations:_landscapemode];
		
		[self refreshClockTick];
		
		[self startAnimation];
		
		[self configureAudioServices];

		
		_appinitialised = true;
		
		
		
	}
	return self;
}

- (void) configureAudioServices {
	
	
	AudioSessionInitialize (NULL, NULL,	NULL,NULL);
	
	//UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
	UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
	AudioSessionSetProperty (kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
	
	
	UInt32 audioOverride =  true;
	AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof (audioOverride), &audioOverride);
	
	AudioSessionSetActive (true);
}

- (IBAction)btnAdjustbrightness:(id)sender
{
	[self playSinglesound:@"clockbuttonhigh"];
	
	if (_currentbrightness < 5) {
		_currentbrightness++;
	} else {
		_currentbrightness = 1;
	}
	
	if (_currentbrightness == 1) {
		self.alpha = 0.2f;
	}
	
	if (_currentbrightness == 2) {
		self.alpha = 0.4f;
	}
	
	if (_currentbrightness == 3) {
		self.alpha = 0.6f;
	}
	
	if (_currentbrightness == 4) {
		self.alpha = 0.8f;
	}
	
	if (_currentbrightness == 5) {
		self.alpha = 1.0f;
	}
}

- (void)refreshAlarmactive_imageview
{
	if (_alarmactive == false) {
		[imgview_alarmactive setImage:[UIImage imageNamed:@"Off.png"]];
		[imgview_alarmactive sizeToFit];
	} else {
		[imgview_alarmactive setImage:[UIImage imageNamed:@"Active.png"]];
		[imgview_alarmactive sizeToFit];
	}
}

- (void) refreshAmpm_imageview
{
	if (_12hourmode == true) {
		if (_ispm == true) {
			[imgview_ampmindicator setImage:[UIImage imageNamed:@"PM.png"]];
		} else {
			[imgview_ampmindicator setImage:[UIImage imageNamed:@"AM.png"]];
		}
		
		[imgview_ampmindicator sizeToFit];
		imgview_ampmindicator.hidden = false;
	} else {
		imgview_ampmindicator.hidden = true;
	}
	
}
   

- (void)loadAlarmsoundlabel:(int)soundnumber
{
	if (soundnumber == 1) {alarmsoundlabel = [UIImage imageNamed:@"Snd1.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 2) {alarmsoundlabel = [UIImage imageNamed:@"Snd2.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 3) {alarmsoundlabel = [UIImage imageNamed:@"Snd3.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 4) {alarmsoundlabel = [UIImage imageNamed:@"Snd4.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 5) {alarmsoundlabel = [UIImage imageNamed:@"Snd5.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 6) {alarmsoundlabel = [UIImage imageNamed:@"Snd6.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 7) {alarmsoundlabel = [UIImage imageNamed:@"Snd7.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 8) {alarmsoundlabel = [UIImage imageNamed:@"Snd8.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 9) {alarmsoundlabel = [UIImage imageNamed:@"Snd9.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 10) {alarmsoundlabel = [UIImage imageNamed:@"Snd10.png"]; [alarmsoundlabel retain];return;}

	
	
	alarmsoundlabel = nil;
}

- (void)saveSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	
	[prefs setBool:_alarmactive forKey:@"alarmactive"];
	[prefs setBool:_12hourmode forKey:@"12hourmode"];
	[prefs setInteger:_currentalarmsound forKey:@"currentalarmsound"];
	[prefs setInteger:_currentalarmhour forKey:@"currentalarmhour"];
	[prefs setInteger:_currentalarmminute forKey:@"currentalarmminute"];
	[prefs setInteger:_currenttimerhour forKey:@"currenttimerhour"];
	[prefs setInteger:_currenttimerminute forKey:@"currenttimerminute"];
	[prefs setInteger:_currenttimersecond forKey:@"currenttimersecond"];
	[prefs setBool:true forKey:@"settingspresent"];
	[prefs synchronize];
}

- (void)loadSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	bool settingspresent = [prefs boolForKey:@"settingspresent"];
	
	if (settingspresent == true) {
	
		_alarmactive = [prefs boolForKey:@"alarmactive"];
		_12hourmode = [prefs boolForKey:@"12hourmode"];
		_currentalarmsound = [prefs integerForKey:@"currentalarmsound"];
		_currentalarmhour = [prefs integerForKey:@"currentalarmhour"];
		_currentalarmminute = [prefs integerForKey:@"currentalarmminute"];
		_currenttimerhour =  [prefs integerForKey:@"currenttimerhour"];
		_currenttimerminute = [prefs integerForKey:@"currenttimerminute"];
		_currenttimersecond = [prefs integerForKey:@"currenttimersecond"];
	}
	
	if (_currentalarmsound == 0) {
		_currentalarmsound = 1;
	}
}

/*
- (void)LoadSounds
{
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:@"clockbutton" ofType:@"WAV" inDirectory:@"/"];
	NSLog (@"Button Sound path: %@", buttonPath);
	CFURLRef sndbuttonURL = (CFURLRef)[[NSURL alloc] initFileURLWithPath:buttonPath];
	AudioServicesCreateSystemSoundID(sndbuttonURL, &_ssbutton);
	
	
	NSString *buttonhighPath = [[NSBundle mainBundle] pathForResource:@"clockbuttonhigh" ofType:@"WAV" inDirectory:@"/"];
	NSLog (@"Button High path: %@", buttonhighPath);
	CFURLRef sndbuttonhighURL = (CFURLRef)[[NSURL alloc] initFileURLWithPath:buttonhighPath];
	AudioServicesCreateSystemSoundID(sndbuttonhighURL, &_ssbuttonhigh);
	 
	NSString *alarm1Path = [[NSBundle mainBundle] pathForResource:@"clocksound1" ofType:@"wav" inDirectory:@"/"];
	NSLog (@"Alarm 1 Sound path: %@", alarm1Path);
	CFURLRef alarm1URL = (CFURLRef)[[NSURL alloc] initFileURLWithPath:alarm1Path];
	AudioServicesCreateSystemSoundID(alarm1URL, &_ssalarm1);
	 
	 
	NSString *alarm2Path = [[NSBundle mainBundle] pathForResource:@"clocksound3" ofType:@"WAV" inDirectory:@"/"];
	NSLog (@"Alarm 2 Sound path: %@", alarm2Path);
	CFURLRef alarm2URL = (CFURLRef)[[NSURL alloc] initFileURLWithPath:alarm2Path];
	AudioServicesCreateSystemSoundID(alarm2URL, &_ssalarm2);
	
	
	
}
*/
 
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		
    }
    return self;
}


- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	_audioplaying = false;
	if (_myaudioplayer != nil)
	{
		NSLog (@"Releasing Audioplayer...");
		[_myaudioplayer release];
	}
}

- (void)playSinglesoundEx:(NSString *)resourcename
{
	if (_audioplaying == true) {
		if (_myaudioplayer != nil)
		{
			[_myaudioplayer stop];
			[_myaudioplayer release];
		}
	}
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		
		_myaudioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];		
		[url release];
		
		[_myaudioplayer setVolume:1];
		[_myaudioplayer setDelegate:self];
		[_myaudioplayer prepareToPlay];
		[_myaudioplayer play];	
		_audioplaying = true;
	}
}

- (void)playSinglesound:(NSString *)resourcename
{
	[self playSinglesoundEx:resourcename];
	
	/*
	AudioServicesDisposeSystemSoundID(_timeannounce);
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		AudioServicesCreateSystemSoundID(sndbuttonURL, &_timeannounce);
		[url release];
	
		AudioServicesPlaySystemSound(_timeannounce);
	}
	*/
}

- (void)playSystemsound:(NSString *)resourcename
{
	AudioServicesDisposeSystemSoundID(_timeannounce);
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		AudioServicesCreateSystemSoundID(sndbuttonURL, &_timeannounce);
		[url release];
		
		AudioServicesPlaySystemSound(_timeannounce);
	}
}

- (void)playCurrentalarmsound
{
	if (_currentalarmsound == 1) {[self playSound:SndAlarmSound1];return;}
	if (_currentalarmsound == 2) {[self playSound:SndAlarmSound2];return;}
	if (_currentalarmsound == 3) {[self playSound:SndAlarmSound3];return;}
	if (_currentalarmsound == 4) {[self playSound:SndAlarmSound4];return;}
	if (_currentalarmsound == 5) {[self playSound:SndAlarmSound5];return;}
	if (_currentalarmsound == 6) {[self playSound:SndAlarmSound6];return;}
	if (_currentalarmsound == 7) {[self playSound:SndAlarmSound7];return;}
	if (_currentalarmsound == 8) {[self playSound:SndAlarmSound8];return;}
	if (_currentalarmsound == 9) {[self playSound:SndAlarmSound9];return;}
	if (_currentalarmsound == 10) {[self playSound:SndAlarmSound10];return;}
}

- (void)playSound:(ClockSound)soundtoplay
{
	if (soundtoplay == SndClockButton)
	{
		[self playSinglesound:@"clockbutton"];
	}
	
	if (soundtoplay == SndClockButtonHigh)
	{
		[self playSinglesound:@"clockbuttonhigh"];
	}
	
	if (soundtoplay == SndAlarmSound1)
	{
		[self playSinglesound:@"clocksound1"];
	}
	
	if (soundtoplay == SndAlarmSound2)
	{
		[self playSinglesound:@"clocksound3"];
	}
	
	if (soundtoplay == SndAlarmSound3)
	{
		[self playSinglesound:@"clocksound4"];
	}
	
	if (soundtoplay == SndAlarmSound4)
	{
		[self playSinglesound:@"clocksound5"];
	}
	
	if (soundtoplay == SndAlarmSound5)
	{
		[self playSinglesound:@"clocksound6"];
	}
	
	if (soundtoplay == SndAlarmSound6)
	{
		[self playSinglesound:@"clocksound7"];
	}
	
	if (soundtoplay == SndAlarmSound7)
	{
		[self playSinglesound:@"clocksound8"];
	}
	
	if (soundtoplay == SndAlarmSound8)
	{
		[self playSinglesound:@"clocksound10"];
	}
	
	if (soundtoplay == SndAlarmSound9)
	{
		[self playSinglesound:@"clocksound11"];
	}
	
	if (soundtoplay == SndAlarmSound10)
	{
		[self playSinglesound:@"clocksound12"];
	}
}

- (void)play12Hournumeric:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"12"];}
	if (numeric == 1) {[self playSinglesound:@"1"];}
	if (numeric == 2) {[self playSinglesound:@"2"];}
	if (numeric == 3) {[self playSinglesound:@"3"];}
	if (numeric == 4) {[self playSinglesound:@"4"];}
	if (numeric == 5) {[self playSinglesound:@"5"];}
	if (numeric == 6) {[self playSinglesound:@"6"];}
	if (numeric == 7) {[self playSinglesound:@"7"];}
	if (numeric == 8) {[self playSinglesound:@"8"];}
	if (numeric == 9) {[self playSinglesound:@"9"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"1"];}
	if (numeric == 14) {[self playSinglesound:@"2"];}
	if (numeric == 15) {[self playSinglesound:@"3"];}
	if (numeric == 16) {[self playSinglesound:@"4"];}
	if (numeric == 17) {[self playSinglesound:@"5"];}
	if (numeric == 18) {[self playSinglesound:@"6"];}
	if (numeric == 19) {[self playSinglesound:@"7"];}
	if (numeric == 20) {[self playSinglesound:@"8"];}
	
	if (numeric == 21) {[self playSinglesound:@"9"];}
	if (numeric == 22) {[self playSinglesound:@"10"];}
	if (numeric == 23) {[self playSinglesound:@"11"];}
}

- (void)playNumericsound:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"0"];}
	if (numeric == 1) {[self playSinglesound:@"01"];}
	if (numeric == 2) {[self playSinglesound:@"02"];}
	if (numeric == 3) {[self playSinglesound:@"03"];}
	if (numeric == 4) {[self playSinglesound:@"04"];}
	if (numeric == 5) {[self playSinglesound:@"05"];}
	if (numeric == 6) {[self playSinglesound:@"06"];}
	if (numeric == 7) {[self playSinglesound:@"07"];}
	if (numeric == 8) {[self playSinglesound:@"08"];}
	if (numeric == 9) {[self playSinglesound:@"09"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"13"];}
	if (numeric == 14) {[self playSinglesound:@"14"];}
	if (numeric == 15) {[self playSinglesound:@"15"];}
	if (numeric == 16) {[self playSinglesound:@"16"];}
	if (numeric == 17) {[self playSinglesound:@"17"];}
	if (numeric == 18) {[self playSinglesound:@"18"];}
	if (numeric == 19) {[self playSinglesound:@"19"];}
	if (numeric == 20) {[self playSinglesound:@"20"];}
	
	if (numeric == 21) {[self playSinglesound:@"21"];}
	if (numeric == 22) {[self playSinglesound:@"22"];}
	if (numeric == 23) {[self playSinglesound:@"23"];}
	if (numeric == 24) {[self playSinglesound:@"24"];}
	if (numeric == 25) {[self playSinglesound:@"25"];}
	if (numeric == 26) {[self playSinglesound:@"26"];}
	if (numeric == 27) {[self playSinglesound:@"27"];}
	if (numeric == 28) {[self playSinglesound:@"28"];}
	if (numeric == 29) {[self playSinglesound:@"29"];}
	if (numeric == 30) {[self playSinglesound:@"30"];}
	
	if (numeric == 31) {[self playSinglesound:@"31"];}
	if (numeric == 32) {[self playSinglesound:@"32"];}
	if (numeric == 33) {[self playSinglesound:@"33"];}
	if (numeric == 34) {[self playSinglesound:@"34"];}
	if (numeric == 35) {[self playSinglesound:@"35"];}
	if (numeric == 36) {[self playSinglesound:@"36"];}
	if (numeric == 37) {[self playSinglesound:@"37"];}
	if (numeric == 38) {[self playSinglesound:@"38"];}
	if (numeric == 39) {[self playSinglesound:@"39"];}
	if (numeric == 40) {[self playSinglesound:@"40"];}
	
	if (numeric == 41) {[self playSinglesound:@"41"];}
	if (numeric == 42) {[self playSinglesound:@"42"];}
	if (numeric == 43) {[self playSinglesound:@"43"];}
	if (numeric == 44) {[self playSinglesound:@"44"];}
	if (numeric == 45) {[self playSinglesound:@"45"];}
	if (numeric == 46) {[self playSinglesound:@"46"];}
	if (numeric == 47) {[self playSinglesound:@"47"];}
	if (numeric == 48) {[self playSinglesound:@"48"];}
	if (numeric == 49) {[self playSinglesound:@"49"];}
	if (numeric == 50) {[self playSinglesound:@"50"];}
	
	if (numeric == 51) {[self playSinglesound:@"51"];}
	if (numeric == 52) {[self playSinglesound:@"52"];}
	if (numeric == 53) {[self playSinglesound:@"53"];}
	if (numeric == 54) {[self playSinglesound:@"54"];}
	if (numeric == 55) {[self playSinglesound:@"55"];}
	if (numeric == 56) {[self playSinglesound:@"56"];}
	if (numeric == 57) {[self playSinglesound:@"57"];}
	if (numeric == 58) {[self playSinglesound:@"58"];}
	if (numeric == 59) {[self playSinglesound:@"59"];}
	
}

- (IBAction)announcebuttonPressed:(id)sender
{
	NSLog (@"Announce button pressed.");
	[self playSound:SndClockButton];
	
	
	if (_timeannouncementplaying == false) {
		_timeannouncementplaying = true;
		[self startAnnouncementTimer];
	}
}

- (void)triggerAnnounce:(id)sender
{
	
	if (_iannouncenumber == 0) {
		[self playSinglesound:@"Time"];
	}
	
	if (_iannouncenumber == 1) {
		if (_12hourmode == true) {
			[self play12Hournumeric:_currenthour];
		} else {
			[self playNumericsound:_currenthour];
		}
		
	}
	
	if (_iannouncenumber == 2) {
		if (_currentminute == 0) {
			if (_12hourmode == true) {
				[self playSinglesound:@"oclock"];
			} else {
				[self playSinglesound:@"hundred"];
			}
		} else {
			[self playNumericsound:_currentminute];
		}
		
	}
	
	if (_12hourmode == true) {
		if (_iannouncenumber == 3) {
			if (_ispm == true) {
				[self playSinglesound:@"pm"];
			} else {
				[self playSinglesound:@"am"];
			}
		}
	}
	
	if (_iannouncenumber >= 4) {
		[announceTimer invalidate];
		_timeannouncementplaying = false;
	}
	
	_iannouncenumber++;
}

- (void)startAnnouncementTimer
{
	_iannouncenumber = 0;
	announceTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1.2 target:self selector:@selector(triggerAnnounce:) userInfo:nil repeats:TRUE];
	[announceTimer retain];
	//clockTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(clockTick:) userInfo:nil repeats:TRUE];
}

- (void)setupImagelocations:(bool)landscape
{
	if (landscape == true) {
		// Set up graphics locations instead of using CGPointMake everytime the screen
		// is painted
		_background_location = CGPointMake(0.0f, -12.0f);
		imgview_lowerleftbar.hidden = true;
		btn_altsound.frame = CGRectMake(740.0f, 353.0f, 101.0f, 84.0f);
		btn_alarm.frame = CGRectMake(740.0f, 445.0f, 101.0f, 84.0f);
		
		imgview_topbar.frame = CGRectMake (260.0f, 0.0f, 521.0f, 61.0f);
		
		if (_timermode == true) {
			btn_alarmtime.frame = CGRectMake(640.0f, 537.0f, 101.0f, 84.0f);
			btn_timerduration.frame = CGRectMake(640.0f, 537.0f, 101.0f, 84.0f);
			
			btn_alarmtime.hidden = true;
			btn_timerduration.hidden = false;
			
			[_scrollynumbers setLastcolumnvisible:!_timermode];
		} else {
			btn_alarmtime.frame = CGRectMake(740.0f, 537.0f, 101.0f, 84.0f);
			btn_timerduration.frame = CGRectMake(740.0f, 537.0f, 101.0f, 84.0f);
			
			btn_alarmtime.hidden = false;
			btn_timerduration.hidden = true;
			
			[_scrollynumbers setLastcolumnvisible:!_timermode];
		}
			
		if (_alarmactive == true) {

		} else {
			[imgview_alarmactive setImage:[UIImage imageNamed:@"Off.png"]];
			[imgview_alarmactive sizeToFit];
		}
		
		btn_alarmmode.frame = CGRectMake(942.0f, 676.0f ,77.0f ,37.0f);
		btn_timermode.frame = CGRectMake(837.0f, 676.0f ,77.0f ,37.0f);
		
		imgview_alarmactive.frame = CGRectMake (840.0f, 455.0f, imgview_alarmactive.frame.size.width, imgview_alarmactive.frame.size.height);
		
	//	_alarmactivelabel_location = CGPointMake(840.0f, 455.0f);
		_alarmsoundlabel_location = CGPointMake(843.0f, 360.0f);
	
		_alert_location = CGPointMake(455.0f, 604.0f);
		 
		btn_secondup.frame = CGRectMake(985.0f, 624.0f ,39.0f ,41.0f);
		btn_seconddown.frame = CGRectMake(945.0f, 624.0f ,39.0f ,41.0f);
		
		btn_starttimer.frame = CGRectMake(642.0f, 626.0f ,77.0f ,37.0f);
		btn_stoptimer.frame = CGRectMake(642.0f, 626.0f ,77.0f ,37.0f);
		
		if (_timermode == true) {
			btn_hourup.frame = CGRectMake(775.0f, 624.0f ,39.0f ,41.0f);
			btn_hourdown.frame = CGRectMake(735.0f, 624.0f ,39.0f ,41.0f);
			btn_minuteup.frame = CGRectMake(880.0f, 624.0f ,39.0f ,41.0f);
			btn_minutedown.frame = CGRectMake(840.0f, 624.0f ,39.0f ,41.0f);
		} else {
			btn_hourup.frame = CGRectMake(875.0f, 624.0f ,39.0f ,41.0f);
			btn_hourdown.frame = CGRectMake(835.0f, 624.0f ,39.0f ,41.0f);
			btn_minuteup.frame = CGRectMake(980.0f, 624.0f ,39.0f ,41.0f);
			btn_minutedown.frame = CGRectMake(940.0f, 624.0f ,39.0f ,41.0f);
		}
		
		_alertsnoozebutton_rect = CGRectMake (310.0f, 604.0f ,140.0f ,109.0f);
		_alertdismissbutton_rect = CGRectMake (175.0f, 604.0f ,140.0f ,109.0f);
		
		[_scrollynumbers setLocation:CGPointMake(190.0f, 600.0f)];
		[_yearnumbers setLocation: 610.0f : 190.0f];
		
		if (_timermode == true) {
			[_alarmnumbers setLocation:CGPointMake(743.0f, 541.0f)];
		} else {
			[_alarmnumbers setLocation:CGPointMake(843.0f, 541.0f)];
		}
				 
	} else {
		
		[_scrollynumbers setLastcolumnvisible:true];
		imgview_lowerleftbar.hidden = false;
		btn_altsound.frame = CGRectMake(170.0f, 583.0f, 101.0f, 84.0f);
		_alarmsoundlabel_location = CGPointMake(273.0f, 590.0f);
		
		imgview_topbar.frame = CGRectMake (260.0f, 0.0f, 265.0f, 61.0f);
		
		btn_alarm.frame = CGRectMake(470.0f, 583.0f, 101.0f, 84.0f);
		
		imgview_alarmactive.frame = CGRectMake (570.0f, 590.0f, imgview_alarmactive.frame.size.width, imgview_alarmactive.frame.size.height);
		
		//_alarmactivelabel_location = CGPointMake(570.0f, 590.0f);
		
		
		[_scrollynumbers setLocation:CGPointMake(190.0f, 852.0f)];
		[_yearnumbers setLocation: 560.0f : 190.0f];

		btn_alarmtime.frame = CGRectMake(170.0f, 677.0f, 101.0f, 84.0f);
		btn_timerduration.frame = CGRectMake(170.0f, 677.0f, 101.0f, 84.0f);
		
		if (_timermode == true) {
			btn_alarmtime.hidden = true;
			btn_timerduration.hidden = false;
		} else {
			btn_alarmtime.hidden = false;
			btn_timerduration.hidden = true;
		}
		
		btn_starttimer.frame = CGRectMake(175.0f, 766.0f ,77.0f ,37.0f);
		btn_stoptimer.frame = CGRectMake(175.0f, 766.0f ,77.0f ,37.0f);

		btn_alarmmode.frame = CGRectMake(627.0f, 680.0f ,77.0f ,37.0f);
		btn_timermode.frame = CGRectMake(627.0f, 722.0f ,77.0f ,37.0f);
		
		[_alarmnumbers setLocation:CGPointMake(273.0f, 681.0f)];
		 
		btn_hourup.frame = CGRectMake(305.0f, 765.0f ,39.0f ,41.0f);
		btn_hourdown.frame = CGRectMake(265.0f, 765.0f ,39.0f ,41.0f);
		btn_minuteup.frame = CGRectMake(410.0f, 765.0f ,39.0f ,41.0f);
		btn_minutedown.frame = CGRectMake(370.0f, 765.0f ,39.0f ,41.0f);
		
		btn_secondup.frame = CGRectMake(515.0f, 765.0f ,39.0f ,41.0f);
		btn_seconddown.frame = CGRectMake(475.0f, 765.0f ,39.0f ,41.0f);
		
		_alert_location = CGPointMake(455.0f, 854.0f);
		_alertsnoozebutton_rect = CGRectMake (310.0f, 854.0f ,140.0f ,109.0f);
		_alertdismissbutton_rect = CGRectMake (175.0f, 854.0f ,140.0f ,109.0f);
	}
	
	if (_timermode == true) {
		imgview_ampmindicator.frame = CGRectMake (645.0f, 469.0f, imgview_ampmindicator.frame.size.width, imgview_ampmindicator.frame.size.height);
	} else {
		imgview_ampmindicator.frame = CGRectMake (645.0f, 494.0f, imgview_ampmindicator.frame.size.width, imgview_ampmindicator.frame.size.height);
	}
	
	if (_timermode == true) {
		btn_secondup.hidden = false;
		btn_seconddown.hidden = false;
		
		if (_timeractive == false) {
			btn_starttimer.hidden = false;
			btn_stoptimer.hidden = true;
		} else {
			btn_stoptimer.hidden = false;
			btn_starttimer.hidden = true;
		}
		
		
	} else {
		btn_secondup.hidden = true;
		btn_seconddown.hidden = true;
		btn_starttimer.hidden = true;
		btn_stoptimer.hidden = true;
		
	}
	
	[self refreshAlarmactive_imageview];
	[self refreshAmpm_imageview];
}

- (void)setLandscapemode:(bool)landscape
{
	if (landscape == true) {
		NSLog(@"Orientation is landscape!");
		_landscapemode = true;
		[self setupImagelocations:_landscapemode];
	} else {
		NSLog(@"Orientation is portrait!");
		_landscapemode = false;
		[self setupImagelocations:_landscapemode];
	}
}

- (void)GetNumericTime
{
	NSDate* date = [NSDate date];
	NSDateComponents *weekdayComponents =[_gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
	
	
	
	//[_formatter setDateFormat:@"HH mm p"];
	
	
	//NSString* strtesttime = [_formatter stringFromDate:date];
	//NSLog (@"Test time: %@", strtesttime);
	
	[_formatter setDateFormat:@"HH"];
	NSString* strhour = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"mm"];
	NSString* strminute = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"MM"];
	NSString* strmonth = [_formatter stringFromDate:date];
	
	
	
	_currentminute = [strminute intValue];
	_currenthour = [strhour intValue];
	_numericweekday = [weekdayComponents weekday];
	_numericmonth = [strmonth intValue];
	
	
	
	if (_currenthour == 0) {_ispm = false;}
	if (_currenthour == 1) {_ispm = false;}
	if (_currenthour == 2) {_ispm = false;}
	if (_currenthour == 3) {_ispm = false;}
	if (_currenthour == 4) {_ispm = false;}
	if (_currenthour == 5) {_ispm = false;}
	if (_currenthour == 6) {_ispm = false;}
	if (_currenthour == 7) {_ispm = false;}
	if (_currenthour == 8) {_ispm = false;}
	if (_currenthour == 9) {_ispm = false;}
	if (_currenthour == 10) {_ispm = false;}
	if (_currenthour == 11) {_ispm = false;}
	if (_currenthour == 12) {_ispm = true;}
	if (_currenthour == 13) {_ispm = true;}
	if (_currenthour == 14) {_ispm = true;}
	if (_currenthour == 15) {_ispm = true;}
	if (_currenthour == 16) {_ispm = true;}
	if (_currenthour == 17) {_ispm = true;}
	if (_currenthour == 18) {_ispm = true;}
	if (_currenthour == 19) {_ispm = true;}
	if (_currenthour == 20) {_ispm = true;}
	if (_currenthour == 21) {_ispm = true;}
	if (_currenthour == 22) {_ispm = true;}
	if (_currenthour == 23) {_ispm = true;}
}

- (void)RefreshAlarmtime
{
	NSNumber *mins;
	NSNumber *hours;
	
	[_strtime setString:@""];
	
	mins = [NSNumber numberWithInt:_currentalarmminute];
	hours = [NSNumber numberWithInt:_currentalarmhour];
	
	if ([hours intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[hours stringValue]];
	[_strtime appendString:@":"];
	
	if ([mins intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[mins stringValue]];
	
	[_alarmnumbers setClockstring:_strtime];

}

- (void)RefreshTimertime
{
	NSNumber *hours;
	NSNumber *mins;
	NSNumber *secs;
	
	[_strtime setString:@""];
	
	hours = [NSNumber numberWithInt:_currenttimerhour];
	mins = [NSNumber numberWithInt:_currenttimerminute];
	secs = [NSNumber numberWithInt:_currenttimersecond];
	
	if ([hours intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[hours stringValue]];
	[_strtime appendString:@":"];
	
	
	if ([mins intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[mins stringValue]];
	[_strtime appendString:@":"];
	
	if ([secs intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[secs stringValue]];
	
	[_alarmnumbers setClockstring:_strtime];
}

- (void)SecondAdjust:(bool)down
{
	if (_timermode == true) {
		if (down == true) {
			_currenttimersecond--;
		
			if (_currenttimersecond <= -1) {
				_currenttimersecond = 59;
			}
		
		} else {
			++_currenttimersecond;
		
			if (_currenttimersecond >= 60) {
				_currenttimersecond = 0;
			}
		}
	
		[self RefreshTimertime];
		[self setNeedsDisplay];
	}
}

- (void)MinuteAdjust:(bool)down
{
	if (_timermode == true) {
		if (down == true) {
			_currenttimerminute--;
			
			if (_currenttimerminute <= -1) {
				_currenttimerminute = 59;
			}
			
		} else {
			++_currenttimerminute;
			
			if (_currenttimerminute >= 60) {
				_currenttimerminute = 0;
			}
		}
		
		[self RefreshTimertime];
		[self setNeedsDisplay];
	} else {
		if (down == true) {
			_currentalarmminute--;
			
			if (_currentalarmminute <= -1) {
				_currentalarmminute = 59;
			}
			
		} else {
			++_currentalarmminute;
			
			if (_currentalarmminute >= 60) {
				_currentalarmminute = 0;
			}
		}
		
		[self RefreshAlarmtime];
		[self setNeedsDisplay];
	}
	
	
}


- (void)HourAdjust:(bool)down
{
	if (_timermode == true) {
		if (down == true) {
			_currenttimerhour--;
			
			if (_currenttimerhour <= -1) {
				_currenttimerhour = 99;
			}
			
		} else {
			++_currenttimerhour;
			
			if (_currenttimerhour >= 100) {
				_currenttimerhour = 0;
			}
		}
		
		[self RefreshTimertime];
		[self setNeedsDisplay];	
	} else {
		if (down == true) {
			_currentalarmhour--;
			
			if (_currentalarmhour <= -1) {
				_currentalarmhour = 23;
			}
			
		} else {
			++_currentalarmhour;
			
			if (_currentalarmhour >= 24) {
				_currentalarmhour = 0;
			}
		}
		
		[self RefreshAlarmtime];
		[self setNeedsDisplay];
	}
	
	
}

- (void)refreshClockTick
{	
	[self GetNumericTime];
	
	NSDate* date = [NSDate date];
	
	
	
	if (_12hourmode == true) {
		[_formatter setDateFormat:@"HH:mm"];
		
		if (_currenthour == 13) {[_formatter setDateFormat:@"1:mm"];}
		if (_currenthour == 14) {[_formatter setDateFormat:@"2:mm"];}		
		if (_currenthour == 15) {[_formatter setDateFormat:@"3:mm"];}
		if (_currenthour == 16) {[_formatter setDateFormat:@"4:mm"];}
		if (_currenthour == 17) {[_formatter setDateFormat:@"5:mm"];}
		if (_currenthour == 18) {[_formatter setDateFormat:@"6:mm"];}
		if (_currenthour == 19) {[_formatter setDateFormat:@"7:mm"];}
		if (_currenthour == 20) {[_formatter setDateFormat:@"8:mm"];}
		if (_currenthour == 21) {[_formatter setDateFormat:@"9:mm"];}
		if (_currenthour == 22) {[_formatter setDateFormat:@"10:mm"];}
		if (_currenthour == 23) {[_formatter setDateFormat:@"11:mm"];}
		if (_currenthour == 0) {[_formatter setDateFormat:@"12:mm"];}
		
		[self refreshAmpm_imageview];
	} else {
		[_formatter setDateFormat:@"HH:mm"];
	}
	
	NSString* str = [_formatter stringFromDate:date];
	[_clocknumbers setClockstring:str];
	
	
	[_formatter setDateFormat:@"ss"];
	str = [_formatter stringFromDate:date];
	[_secondnumbers setClockstring:str];
	
	[_formatter setDateFormat:@"d"];
	str = [_formatter stringFromDate:date];
	[_daynumbers setClockstring:str];
	
	[_formatter setDateFormat:@"yyyy"];
	str = [_formatter stringFromDate:date];
	[_yearnumbers setClockstring:str];
	
	[_formatter setDateFormat:@"yyyyMM.d"];
	str = [_formatter stringFromDate:date];
	[_stardatenumbers setClockstring:str];
	
	[_formatter setDateFormat:@"EEEE"];
	str = [_formatter stringFromDate:date];
	[_daystemplate setNumericWeekday:_numericweekday];
	
	[_formatter setDateFormat:@"MMMM"];
	str = [_formatter stringFromDate:date];
	[_monthstemplate setNumericMonth:_numericmonth];
	
	
	if (_alarmactive == true) {
		
		if (_userdismissedalert == false) {
			if (_currentalarmhour == _currenthour)
			{
				if (_currentalarmminute == _currentminute) {
					
					if (_alertactive == false) {
						_alertactive = true;
					}
					
				}
			}
		}
		
	}
	
	if (_currentalarmminute != _currentminute) {
		_userdismissedalert = false;
	}
	
}

- (void)clockTick:(id)sender
{
	[self refreshClockTick];
	[self setNeedsDisplay];
}

- (void)snoozeTick:(id)sender
{
	_alertactive = true;
}

- (void)drawView:(id)sender
{
	if (_appinitialised == false) {
		return;
	}
	
	[self refreshClockTick];
	
	_alarmbuttonrepeat++;
	
	if (_alarmbuttonrepeat > _repeatspeed) {
		_alarmbuttonrepeat = 0;
	}
	
	
	_alertrepeatcount++;
	
	if (_alertrepeatcount > 5) {
		_alertrepeatcount = 0;
		
		if (_alerttextvisible == true) {
			_alerttextvisible = false;
			
		} else {
			_alerttextvisible = true;
			
			if (_alertactive == true) {			
				[self playCurrentalarmsound];
			}
			
		}
		
		[self setNeedsDisplay];
	}
	
	[_scrollynumbers Update];
	
	[self setNeedsDisplay];
}


- (void)startAnimation
{
	animationTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.3 target:self selector:@selector(drawView:) userInfo:nil repeats:TRUE];
	[animationTimer retain];
}

- (void)startSnooze
{
	// Set the snooze timer for 10 minutes
	snoozeTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)600 target:self selector:@selector(snoozeTick:) userInfo:nil repeats:FALSE];
	[snoozeTimer retain];
}

- (void)drawRect:(CGRect)rect {
	
	
	if (_appinitialised == false) {
		return;
	}
	
    // Drawing code
	
	
	if (_alertactive == false) {
		[_scrollynumbers Paint];
	}
	
	
	[_clocknumbers PaintString];
	[_secondnumbers PaintString];
	[_alarmnumbers PaintString];
	[_daynumbers PaintString];
	[_yearnumbers PaintString];
	[_daystemplate PaintString];
	[_monthstemplate PaintString];
	[_stardatenumbers PaintString];
	
	
	
	
	if (alarmsoundlabel != nil) {
		if (alarmsoundlabel != nil) {
			[alarmsoundlabel drawAtPoint:_alarmsoundlabel_location];
		}
	}
	
	
	
	if (_alertactive == true) {
		
		if (_snoozebuttondown == true) {
			if (alertsnoozehigh != nil) {
				[alertsnoozehigh drawAtPoint:_alertsnoozebutton_rect.origin];
			}
		} else {
			if (alertsnoozenorm != nil) {
				[alertsnoozenorm drawAtPoint:_alertsnoozebutton_rect.origin];
			}
		}
		
		
		if (_dismissbuttondown == true) {
			if (alertdismisshigh != nil) {
				[alertdismisshigh drawAtPoint:_alertdismissbutton_rect.origin];
			}
		} else {
			
			if (alertdismissnorm != nil) {
				[alertdismissnorm drawAtPoint:_alertdismissbutton_rect.origin];
			}
		}
		
		
		if (_alerttextvisible == true) {
			
			if (alert != nil) {
				[alert drawAtPoint:_alert_location];
			}
		}
	}
}

- (bool)isPointWithinRectEx:(CGFloat)x:(CGFloat)y:(CGRect)rect
{
	bool hmatch = false;
	bool vmatch = false;
	
	if (x >= rect.origin.x && x<= (rect.origin.x+rect.size.width)) {
		hmatch = true;
	}
	
	if (y >= rect.origin.y && y<= (rect.origin.y+rect.size.height)) {
		vmatch = true;
	}
	
	if (vmatch == true && hmatch == true) {
		return true;
	} else {
		return false;
	}
}

- (void)Allbuttonsreleased
{
	_lastbuttontouched = None;	
	_snoozebuttondown = false;
	_dismissbuttondown = false;
}


- (IBAction)alarmAdjustcancel:(id)sender
{
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		_alarmadjusttimer = nil;
	}
}

- (void)alarmAdjust:(AlarmAdjustMode)adjustmode
{
	if (_alarmadjustedbytimer == false) {
		if (adjustmode == AdjHourUp) {
			[self HourAdjust:false];
		}
		if (adjustmode == AdjHourDown) {
			[self HourAdjust:true];
		}
		if (adjustmode == AdjMinuteUp) {
			[self MinuteAdjust:false];
		}
		if (adjustmode == AdjMinuteDown) {
			[self MinuteAdjust:true];
		}
		if (adjustmode == AdjSecondUp) {
			[self SecondAdjust:false];
		}
		if (adjustmode == AdjSecondDown) {
			[self SecondAdjust:true];
		}
		
	}
	
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		_alarmadjusttimer = nil;
	}
}

- (void) alarmAdjusttick:(id)sender
{
	if (_alarmadjustmode == AdjHourUp)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:false];
	}
	
	if (_alarmadjustmode == AdjHourDown)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:true];
	}
	
	if (_alarmadjustmode == AdjMinuteUp)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:false];
	}
	
	if (_alarmadjustmode == AdjMinuteDown)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:true];
	}
	
	if (_alarmadjustmode == AdjSecondUp)
	{
		_alarmadjustedbytimer = true;
		[self SecondAdjust:false];
	}
	
	if (_alarmadjustmode == AdjSecondDown)
	{
		_alarmadjustedbytimer = true;
		[self SecondAdjust:true];
	}
}

- (void)startAlarmadjust
{
	//[self HourAdjust:false];
	_alarmadjustedbytimer = false;
	_alarmadjusttimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.3 target:self selector:@selector(alarmAdjusttick:) userInfo:nil repeats:TRUE];
	[_alarmadjusttimer retain];
}

- (void)timerTick:(id)sender {
	
	
	_currenttimersecond--;
	
	if (_currenttimersecond < 0) {
		_currenttimersecond = 59;
		
		_currenttimerminute--;
		
		if (_currenttimerminute < 0) {
			_currenttimerminute = 59;
			
			_currenttimerhour--;
			
			if (_currenttimerhour < 0) {
				
				// Timer has reached zero - sound the alert and reset!
				[self stopTimer];
				
				_currenttimerhour = _backuptimerhour;
				_currenttimerminute = _backuptimerminute;
				_currenttimersecond = _backuptimersecond;
				
				_timermode = false;
				
				[self RefreshAlarmtime];
				[self setupImagelocations:_landscapemode];
				[self setNeedsDisplay];
				
				if (_landscapemode == true) {
					[_scrollynumbers setLastcolumnvisible:!_timermode];
				}
				
				_alertactive = true;
				
				
			}
		}
	}
	
	if (_timermode == true) {
		[self RefreshTimertime];
		[self setNeedsDisplay];
	}
}

- (void)startTimer {
	
	if (_timeractive == false) {
		
		_backuptimerhour = _currenttimerhour;
		_backuptimerminute = _currenttimerminute;
		_backuptimersecond = _currenttimersecond;
		
		_timertimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(timerTick:) userInfo:nil repeats:TRUE];
		[_timertimer retain];
		_timeractive = true;
	}
}

- (void)stopTimer {
	
	if (_timeractive == true) {
		[_timertimer invalidate];
		_timertimer = nil;
		_timeractive = false;
	}
}

- (IBAction)altAlarmsoundbuttontriggered:(id)sender
{
	
}

- (IBAction)altAlarmsoundbuttonreleased:(id)sender
{
	[self playCurrentalarmsound];
	
	[self setNeedsDisplay];
	
}

- (IBAction)hourUpbuttontriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjHourUp;
	[self startAlarmadjust];
}

- (IBAction)hourDownbuttontriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjHourDown;
	[self startAlarmadjust];	
}

- (IBAction)minuteUpbuttontriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjMinuteUp;
	[self startAlarmadjust];
}

- (IBAction)minuteDownbuttontriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjMinuteDown;
	[self startAlarmadjust];
}

- (IBAction)secondUpbuttontriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjSecondUp;
	[self startAlarmadjust];
}

- (IBAction)secondDownbuttontriggered:(id)sender
{
	[self playSound:SndClockButtonHigh];
	_alarmadjustmode = AdjSecondDown;
	[self startAlarmadjust];
}

- (IBAction)hourUpbuttonreleased:(id)sender
{
	[self alarmAdjust:AdjHourUp];
}

- (IBAction)hourDownbuttonreleased:(id)sender
{
	[self alarmAdjust:AdjHourDown];
}

- (IBAction)minuteUpbuttonreleased:(id)sender
{
	[self alarmAdjust:AdjMinuteUp];
}

- (IBAction)minuteDownbuttonreleased:(id)sender
{
	[self alarmAdjust:AdjMinuteDown];
}

- (IBAction)secondUpbuttonreleased:(id)sender
{
	[self alarmAdjust:AdjSecondUp];
}

- (IBAction)secondDownbuttonreleased:(id)sender
{
	[self alarmAdjust:AdjSecondDown];
}

- (IBAction)alarmSoundBackbuttontriggered:(id)sender
{
	_currentalarmsound--;
	
	if (_currentalarmsound < 1) {
		_currentalarmsound = 10;
	}
	
	[self loadAlarmsoundlabel:_currentalarmsound];
	[self setNeedsDisplay];
	[self playCurrentalarmsound];
}

- (IBAction)alarmSoundNextbuttontriggered:(id)sender
{
	_currentalarmsound++;
	
	if (_currentalarmsound > 10) {
		_currentalarmsound = 1;
	}
	
	
	[self loadAlarmsoundlabel:_currentalarmsound];
	[self setNeedsDisplay];
	[self playCurrentalarmsound];
}


- (IBAction)alarmOnOffbuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
}

- (IBAction)alarmOnOffbuttonreleased:(id)sender
{
	if (_alarmactive == false) {
		_alarmactive = true;
	} else {
		_alarmactive = false;
	}
	
	
	[self refreshAlarmactive_imageview];
	[self setNeedsDisplay];
}

- (IBAction)hourModebuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
}

- (IBAction)hourModebuttonreleased:(id)sender
{
	if (_12hourmode == false) {
		_12hourmode = true;
	} else {
		_12hourmode = false;
	}
	
	[self refreshAmpm_imageview];
	[self setNeedsDisplay];
}

- (IBAction)alarmtimermodebuttonreleased:(id)sender
{
	[self playSound:SndClockButton];
	
	if (_timermode == false){
		_timermode = true;
		[self RefreshTimertime];
	} else {
		_timermode = false;
		[self RefreshAlarmtime];
	}
	
	// Reposition the buttons if necessary
	[self setupImagelocations:_landscapemode];
	
	if (_landscapemode == true) {
		[_scrollynumbers setLastcolumnvisible:!_timermode];
	}
	
	[self setNeedsDisplay];
}

- (IBAction)starttimerbuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
}

- (IBAction)starttimerbuttonreleased:(id)sender
{
	[self startTimer];
	[self setupImagelocations:_landscapemode];
	[self setNeedsDisplay];
}

- (IBAction)stoptimerbuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
}

- (IBAction)stoptimerbuttonreleased:(id)sender
{
	[self stopTimer];
	[self setupImagelocations:_landscapemode];
	[self setNeedsDisplay];	
}

- (IBAction)timerModebuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
	
	_timermode = true;
	[self RefreshTimertime];

	[self setupImagelocations:_landscapemode];
	
	if (_landscapemode == true) {
		[_scrollynumbers setLastcolumnvisible:!_timermode];
	}
	
	[self setNeedsDisplay];
	
}

- (IBAction)alarmModebuttontriggered:(id)sender
{
	[self playSound:SndClockButton];
	
	_timermode = false;
	[self RefreshAlarmtime];
	
	[self setupImagelocations:_landscapemode];
	
	if (_landscapemode == true) {
		[_scrollynumbers setLastcolumnvisible:!_timermode];
	}
	
	[self setNeedsDisplay];
	
}

- (void)checkButtonmoved:(CGFloat)x:(CGFloat)y:(bool)checkreleased
{
	if (checkreleased == true) {
		// Alarm on off button
		
		if (_alertactive == true) {
			// snooze down button
			if ([self isPointWithinRectEx:x :y :_alertsnoozebutton_rect] == false && _lastbuttontouched == AlertSnooze) {
				_snoozebuttondown = false;
				_alertactive = false;
				_snoozemode = true;
				_userdismissedalert = true;
				[self startSnooze];
				[self setNeedsDisplay];
			}
			
			
			// dismiss down button
			if ([self isPointWithinRectEx:x :y :_alertdismissbutton_rect] == false && _lastbuttontouched == AlertDismiss) {
				_dismissbuttondown = false;
				_alertactive = false;
				_userdismissedalert = true;
				[self setNeedsDisplay];
			}
		}
	}
}

- (void)checkButtontouched:(CGFloat)x:(CGFloat)y:(bool)checkreleased
{
	if (checkreleased == true) {
		
		if (_alertactive == true) {
			// snooze down button
			if ([self isPointWithinRectEx:x :y :_alertsnoozebutton_rect] == true) {
				_snoozebuttondown = false;
				_alertactive = false;
				_snoozemode = true;
				_userdismissedalert = true;
				[self startSnooze];
				[self setNeedsDisplay];
			}
			
			
			// dismiss down button
			if ([self isPointWithinRectEx:x :y :_alertdismissbutton_rect] == true) {
				_dismissbuttondown = false;
				_alertactive = false;
				_userdismissedalert = true;
				[self setNeedsDisplay];
			}
		}

		
	} else {
		
		if (_alertactive == true) {
			// snooze down button
			if ([self isPointWithinRectEx:x :y :_alertsnoozebutton_rect] == true) {
				_snoozebuttondown = true;
				_lastbuttontouched = AlertSnooze;
				[self playSound:SndClockButton];

				[self setNeedsDisplay];
			}
			
			// dismiss down button
			if ([self isPointWithinRectEx:x :y :_alertdismissbutton_rect] == true) {
				_dismissbuttondown = true;
				_lastbuttontouched = AlertDismiss;
				[self playSound:SndClockButton];

				[self setNeedsDisplay];
			}
		}
		
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	//NSLog(@"Touches began!!");
	
	NSArray *allTouches = [touches allObjects];
	UITouch *currentTouch;
	CGPoint currentPoint;
	
	//NSLog (@"Number of touches in array: %i", [allTouches count]);
	
	int t = 0;
	
	for (t=0;t<[allTouches count];++t)
	{
		currentTouch = [allTouches objectAtIndex:t];
		currentPoint = [currentTouch locationInView:self];
		
		//NSLog (@"Location of touch %i: %f, %f", t, currentPoint.x, currentPoint.y);
		
		[self checkButtontouched:currentPoint.x: currentPoint.y: false];
		
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	//NSLog(@"Touches moved!!");
	
	NSArray *allTouches = [touches allObjects];
	UITouch *currentTouch;
	CGPoint currentPoint;
	
	//NSLog (@"Number of movetouches in array: %i", [allTouches count]);
	
	int t = 0;
	
	for (t=0;t<[allTouches count];++t)
	{
		currentTouch = [allTouches objectAtIndex:t];
		currentPoint = [currentTouch locationInView:self];
		
		[self checkButtonmoved:currentPoint.x: currentPoint.y: true];
		//[self checkButtontouched:currentPoint.x: currentPoint.y: true];
		//NSLog (@"Location of movetouch %i: %f, %f", t, currentPoint.x, currentPoint.y);
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	//NSLog(@"Touches ended.");
	_alarmtoggleset = false;
	_hourmodetoggleset = false;
	_soundtoggleset = false;
	//NSLog(@"Touches began!!");
	
	NSArray *allTouches = [touches allObjects];
	UITouch *currentTouch;
	CGPoint currentPoint;
	
	[self Allbuttonsreleased];
	//NSLog (@"Number of touches in array: %i", [allTouches count]);
	
	int t = 0;
	
	for (t=0;t<[allTouches count];++t)
	{
		currentTouch = [allTouches objectAtIndex:t];
		currentPoint = [currentTouch locationInView:self];
		
		//NSLog (@"Location of touch %i: %f, %f", t, currentPoint.x, currentPoint.y);
		[self checkButtontouched:currentPoint.x: currentPoint.y: true];
		
	}
}

/////////////////////////////////////////////////////////////////////////
// LOCAL NOTIFICATION STUFF
/////////////////////////////////////////////////////////////////////////

- (void)scheduleGenericalarm
{
	if (_alarmactive == false) {
		return;
	}
	
	NSDate *now = [NSDate date];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
	
	
	NSDateComponents *alarm_components = [gregorian components:unitFlags fromDate:now];
	
	//int alarmday = [alarm_components day];
	//int alarmmonth = [alarm_components month];
	//int alarmyear = [alarm_components year];
	//int alarmhour = _currentalarmhour;
	//int alarmminute = _currentalarmminute;
	
	[alarm_components setHour:_currentalarmhour];
	[alarm_components setMinute:_currentalarmminute];
	
	//NSLog (@"Alarm Day: %i", alarmday);
	//NSLog (@"Alarm Month: %i", alarmmonth);
	//NSLog (@"Alarm Year: %i", alarmyear);
	//NSLog (@"Alarm Hour: %i", alarmhour);
	//NSLog (@"Alarm Minute: %i", alarmminute);
	
	
	NSDate *alarm_today = [gregorian dateFromComponents:alarm_components];
	
	//NSLog (@"Alarm Date is: %@", alarm_today);
	
	NSDateComponents *addcomp = [[NSDateComponents alloc] init];
	addcomp.day = 1;
	
	NSDate *alarm_tomorrow = [gregorian dateByAddingComponents:addcomp toDate:alarm_today options:0];
	
	//NSLog (@"Alarm Tomorrow: %@", alarm_tomorrow);
	
	if ([alarm_today timeIntervalSinceDate:now] >= 0) {
		//NSLog (@"Alarm today is in the future");
		
		[self scheduleAlarmForDate:alarm_today];
	} else {
		//NSLog (@"Alarm today is in the past");
		
		[self scheduleAlarmForDate:alarm_tomorrow];
	}
	
	//NSDate *now = [NSDate date];
	//NSDate *future = [now dateByAddingTimeInterval:10];
	
	
	
	[gregorian release];
	[addcomp release];
	//[self scheduleAlarmForDate:future];
}

- (void)clearAllLocalnotifications
{
	
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
		NSLog (@"Local notifications cleared.");
	}
}

- (void)scheduleAlarmForDate:(NSDate*)theDate
{
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
	}
	
	// Create a new notification
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	
	if (alarm)
	{
		alarm.fireDate = theDate;
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		alarm.repeatInterval = NSDayCalendarUnit;
		
		//if (_usingsound1 == true) {
		//alarm.soundName = @"clocksound1repeat.wav";
		//} else {
		alarm.soundName = @"clocksound3-repeat.WAV";
		//}
		
		alarm.alertBody = @"Alarm";
		
		[app scheduleLocalNotification:alarm];
	}
}


- (void)dealloc {
    [super dealloc];
}


@end
