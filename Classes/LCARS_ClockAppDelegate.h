//
//  LCARS_ClockAppDelegate.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LCARS_ClockViewController;

@interface LCARS_ClockAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    LCARS_ClockViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet LCARS_ClockViewController *viewController;

@end

