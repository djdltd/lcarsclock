//
//  LCARS_ClockViewController.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import "LCARS_ClockViewController.h"

@implementation LCARS_ClockViewController
@synthesize clockview;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {

	[[UIApplication sharedApplication] setStatusBarHidden:YES];
    [super viewDidLoad];
}

- (void) saveSettings {
	[clockview saveSettings];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
	
	if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
		[clockview setLandscapemode:true];
	}
	
	if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
		[clockview setLandscapemode:true];		
	}
	
	if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
		[clockview setLandscapemode:false];	
	}
	
	if (interfaceOrientation == UIInterfaceOrientationPortrait) {
		[clockview setLandscapemode:false];	
	}
	
	return YES;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	
}


- (void)dealloc {
    [super dealloc];
}

@end
