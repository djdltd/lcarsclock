//
//  ScrollyNumbers.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameLocation.h"

@interface ScrollyNumbers : NSObject {
	NSMutableArray *_blueFrames;
	NSMutableArray *_whiteFrames;
	NSMutableArray *_frameLocations;
	
	UIImage *frame;
	FrameLocation *curframe;
	CGFloat _xscreenoffset;
	CGFloat _yscreenoffset;
	int _endframe;
	CGRect _entireRect;
	bool _whitemode;
	int _flashcount;
	bool _flashmode;
	bool _flashon;
	int _whiteframetarget;
	CGPoint _location;
	bool _lastcolvisible;
}

- (void) setLastcolumnvisible:(bool)bvisible;
- (void) Initialise;
- (void) InitialiseFrames:(bool)whiteFrames;
- (void) Paint;
- (void) Update;
- (CGRect) getEntirerect;
- (bool) isFramelastcolumn:(int)framenumber;
- (CGPoint) getActualframelocation:(int)FrameNumber;
- (void) setLocation:(CGPoint)location;
@end
