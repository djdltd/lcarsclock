//
//  FrameLocation.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "FrameLocation.h"


@implementation FrameLocation

- (CGPoint) getLocation
{
	return location;
}

- (void) setLocation:(CGPoint)loc
{
	location = loc;
}

@end
